# Seminar: Clean Code in C++
## September 16, 2019. Uppsala, Sweden.

The concept Code Smell is an analogy with the presence of mould in a house. The interpretation
is that it will just getting worse, the longer it takes to start dealing with the problem.
Clean Code is the process of step-wise and frequent actions to mitigate the problem that
program code over time gets harder to modify, because of small changes might result in
severe bugs.

This seminar provides an introduction to the topic with a focus towards C++ and shows
how C++ 11/14/17 can support and simplify the task of cleaning up code.

The seminar is presented by Jens Riboe, CEO of Ribomation AB. Jens have been programming
professionally since the mid 80s, in many different programming languages, on different platforms,
for both large and small corporation, and within as well as outside Sweden.


