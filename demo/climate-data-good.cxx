#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <unordered_map>
#include <vector>
#include <regex>
#include <stdexcept>
#include <tuple>
#include <numeric>

using namespace std;
using namespace std::literals;
using ClimateData = unordered_map<int, vector<float>>;

auto loadClimateData(istream& input) -> ClimateData;
auto aggregate(ClimateData const& data) -> map<int, double>;
auto extractDateAndTemperature(string const& line) -> tuple<int, float>;
auto toCelsius(double f) -> float;

int main() {
    auto filename = "../climate-data.txt"s;
    auto file     = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    for (auto[month, avgtemp] : aggregate(loadClimateData(file))) {
        cout << setw(2) << setfill('0') << month << ": "
             << setw(5) << setfill(' ') << setprecision(3) << avgtemp << " C"
             << endl;
    }
    return 0;
}

auto loadClimateData(istream& file) -> ClimateData {
    auto      temperaturesPerMonth = ClimateData{};
    for (auto line = ""s; getline(file, line);) {
        if (line.starts_with("STN"s)) continue;
        auto[month, celsius] = extractDateAndTemperature(line);
        temperaturesPerMonth[month].push_back(celsius);
    }
    return temperaturesPerMonth;
}

auto extractDateAndTemperature(string const& line) -> tuple<int, float> {
    // STN--- WBAN   YEARMODA    TEMP       DEWP ...
    // 141200 99999  20180101    49.0 24    44.5 ...
    auto spaces = regex{"\\s+"};
    auto token  = sregex_token_iterator{begin(line), end(line), spaces, -1};
    ++token; //skip STN
    ++token; //skip WBAN
    auto YEARMODA   = *token++;
    auto TEMP       = *token;
    auto month      = stoi(YEARMODA.str().substr(4, 2));
    auto fahrenheit = stod(TEMP);
    return make_tuple(month, toCelsius(fahrenheit));
}

auto aggregate(unordered_map<int, vector<float>> const& data) -> map<int, double> {
    auto result = map<int, double>{};
    for (auto[month, T] : data) {
        result[month] = accumulate(begin(T), end(T), 0.0) / T.size();
    }
    return result;
}

auto toCelsius(double f) -> float {
    return static_cast<float>((f - 32.0) / 1.8);
}
