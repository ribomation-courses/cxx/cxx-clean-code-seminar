#include <iostream>
#include <string>
#include <unordered_map>
#include <list>

void bad_code() {
    std::cout << "-- bad code --\n";
    std::unordered_map<std::string, std::list<unsigned>> tbl;

    std::list<unsigned> first;
    first.push_back(10); first.push_back(20); first.push_back(30);
    tbl["first"] = first;

    std::list<unsigned> second;
    second.push_back(5); second.push_back(15); second.push_back(25);
    tbl["seconds"] = second;

    std::unordered_map<std::string, std::list<unsigned>>::iterator it;
    it = tbl.begin();
    while (it != tbl.end()) {
        std::cout << it->first << ": ";
        std::list<unsigned>::iterator it2 = it->second.begin();
        while (it2 != it->second.end()) {
            std::cout << *it2 << " "; ++it2;
        }
        std::cout << std::endl; ++it;
    }
}

void good_code() {
    using namespace std;
    using namespace std::literals;
    cout << "-- good code --\n";

    auto tbl = unordered_map<string, list<unsigned>>{
            {"first"s,  {10, 20, 30}},
            {"second"s, {5,  15, 25}},
    };

    for (auto const& [name, numbers] : tbl) {
        cout << name << ": ";
        for (auto n : numbers) cout << n << " ";
        cout << endl;
    }
}

int main() {
    bad_code();
    good_code();
    return 0;
}
